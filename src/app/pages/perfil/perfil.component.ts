import { Component, OnInit } from '@angular/core';
import { Rol } from 'src/app/_model/rol';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { LoginService } from './../../_service/login.service';
import { Cliente } from 'src/app/_model/cliente';
import { ClienteService } from 'src/app/_service/cliente.service';
import { DomSanitizer } from '@angular/platform-browser';


@Component({
  selector: 'app-perfil',
  templateUrl: './perfil.component.html',
  styleUrls: ['./perfil.component.css']
})
export class PerfilComponent implements OnInit {
  roles: Rol[] = [];
  displayedColumns = [ 'nombre'];
  cliente: Cliente;
  userName: string;
  imagenData: any;
  imagenEstado: boolean = false;

  constructor(
    private usuarioService: UsuarioService,
    private clienteService: ClienteService,
    private loginService: LoginService,
    private sanitization: DomSanitizer
  ) { }

  ngOnInit() {
        let tokenUsuario = this.usuarioService.obtenerTokenUsuario();
        const helper = new JwtHelperService();
        const decodedToken = helper.decodeToken(tokenUsuario);
        
        console.log(decodedToken.user_name);
        

        this.userName = decodedToken.user_name;
        let count = 0;
        for (let rol in decodedToken.authorities) {
          
          let roluser : Rol = new Rol;
          roluser.idRol = count;
          roluser.nombre = decodedToken.authorities[count];
          
          this.roles.push(roluser)
          count++;
        }
     
        
      this.usuarioService.obtenerDetalleUsuario(decodedToken.user_name).subscribe(data => {
          console.log(data);

          this.cliente  = data.cliente;   
          this.clienteService.obtenerFoto(data.cliente.idCliente).subscribe(data => {
            if(data.size > 0){
              this.convertir(data);
            }        
          })
      });

  }

  convertir(data: any) {
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let base64 = reader.result;      
      //console.log(base64);
      //this.imagenData = base64;            
      //this.imagenEstado = true;
      this.setear(base64);
    }
  }

  setear(x: any) {
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(x);
    this.imagenEstado = true;
  }

}
