import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ClienteService } from './../../../_service/cliente.service';
import { Cliente } from './../../../_model/cliente';
import { Component, OnInit, Inject } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';
import { Usuario } from 'src/app/_model/usuario';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { FormGroup, FormBuilder, Validators, FormControl } from '@angular/forms';

@Component({
  selector: 'app-cliente-dialogo',
  templateUrl: './cliente-dialogo.component.html',
  styleUrls: ['./cliente-dialogo.component.css']
})
export class ClienteDialogoComponent implements OnInit {
  usuario: Usuario;
  cliente: Cliente;
  imagenData: any;
  imagenEstado: boolean = false;
  selectedFiles: FileList;
  currentFileUpload: File;
  labelFile: string;
  maxFecha: Date = new Date();
  nuevaClave: string;
  clientFormGroup: FormGroup;
  clienteNuevo: boolean = false;

  constructor(
    private formBuilder: FormBuilder,
    private dialogRef: MatDialogRef<ClienteDialogoComponent>, 
    @Inject(MAT_DIALOG_DATA) public data: Cliente, 
    private clienteService: ClienteService,
    private usuarioService: UsuarioService,
    private sanitization: DomSanitizer) { }

  ngOnInit() {

    this.clientFormGroup = this.formBuilder.group({
      clienteNombre: ['', Validators.required],
      clienteApellido: ['', Validators.required],
      clienteDni: ['', Validators.required],
      clienteFecha: ['', Validators.required],
      usuarioNombre: ['', Validators.required],
      usuarioClave: [''],
    });


    this.usuario = new Usuario();
    this.cliente = new Cliente();
    this.usuario.cliente = new Cliente();
    if (this.data.idCliente > 0) {

      this.clienteNuevo = false;
     
      this.cliente.idCliente = this.data.idCliente;
      this.usuarioService.obtenerDetalleUsuarioPorIdCliente(this.data.idCliente).subscribe(data => {
        this.usuario = data;
        this.clienteService.obtenerFoto(data.cliente.idCliente).subscribe(data => {
         if(data.size > 0){
            this.convertir(data);
          }        
        })
      });

    }else{
      this.clientFormGroup.controls['usuarioClave'].setValidators([Validators.required]);              
    
      this.clienteNuevo = true;
      
    }
  }

  convertir(data: any) {
    let reader = new FileReader();
    reader.readAsDataURL(data);
    reader.onloadend = () => {
      let base64 = reader.result;      
      //console.log(base64);
      //this.imagenData = base64;            
      //this.imagenEstado = true;
      this.setear(base64);
    }
  }

  setear(x: any) {
    this.imagenData = this.sanitization.bypassSecurityTrustResourceUrl(x);
    this.imagenEstado = true;
  }

  operar() {

    if (this.selectedFiles != null) {
      this.currentFileUpload = this.selectedFiles.item(0);
    } else {
      this.currentFileUpload = new File([""], "blanco");
    }

    if (this.usuario != null && this.usuario.cliente.idCliente > 0) {
      if (this.nuevaClave != null){
        this.usuario.clave = this.nuevaClave;
      }
        this.clienteService.modificar(this.usuario, this.currentFileUpload).subscribe(data => {
          this.clienteService.listar().subscribe(clientes => {
            this.clienteService.clienteCambio.next(clientes);
            this.clienteService.mensajeCambio.next("Se modifico");
          });
        });
    } else {
      if (this.nuevaClave != null){
        this.usuario.clave = this.nuevaClave;
      }

        this.clienteService.registrar(this.usuario, this.currentFileUpload).subscribe(data => {
          this.clienteService.listar().subscribe(clientes => {
            this.clienteService.clienteCambio.next(clientes);
            this.clienteService.mensajeCambio.next("Se registro");
          });
        });
    }
    this.dialogRef.close();
  }

  selectFile(e: any) {
    console.log(e);
    this.labelFile = e.target.files[0].name;
    this.selectedFiles = e.target.files;
  }

  cancelar() {
    this.dialogRef.close();
  }

}
