import { ClienteDialogoComponent } from './cliente-dialogo/cliente-dialogo.component';
import { ClienteService } from './../../_service/cliente.service';
import { Cliente } from './../../_model/cliente';
import { Component, OnInit, ViewChild } from '@angular/core';
import { MatTableDataSource, MatPaginator, MatSort, MatDialog, MatSnackBar } from '@angular/material';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-cliente',
  templateUrl: './cliente.component.html',
  styleUrls: ['./cliente.component.css']
})
export class ClienteComponent implements OnInit {
  cantidad: number;
  dataSource: MatTableDataSource<Cliente>;
  displayedColumns = ['idCliente', 'nombres', 'acciones'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort, { static: true }) sort: MatSort;

  constructor(private clienteService: ClienteService,
    private usuarioService: UsuarioService,     
    private dialog: MatDialog, 
    private snackBar: MatSnackBar) { }

  ngOnInit() {
    this.clienteService.mensajeCambio.subscribe(data => {
      this.snackBar.open(data, 'INFO', {
        duration: 2000
      });
    });

    this.clienteService.clienteCambio.subscribe(data => {
 
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
    
    this.clienteService.listar().subscribe(data => {
       
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  filter(x: string) {
    this.dataSource.filter = x.trim().toLowerCase();
  }

  openDialog(cliente?: Cliente) {
    let com = cliente != null ? cliente : new Cliente();
    this.dialog.open(ClienteDialogoComponent, {
      width: '250px',
      data: com
    });
  }

  eliminar(cliente: Cliente) {

    let tokenUsuario = this.usuarioService.obtenerTokenUsuario();
    const helper = new JwtHelperService();
    const decodedToken = helper.decodeToken(tokenUsuario);
    

    this.usuarioService.obtenerDetalleUsuario(decodedToken.user_name).subscribe(data => {
      console.log(data);

      if(data.cliente.idCliente != cliente.idCliente){

        this.clienteService.eliminar(cliente.idCliente).subscribe(data => {
          this.clienteService.listar().subscribe(clientes => {
            this.clienteService.clienteCambio.next(clientes);
            this.clienteService.mensajeCambio.next("Se elimino");
          });
        });
      
      }else{

        this.clienteService.mensajeCambio.next("No puedes eliminar tu propio usuario");

      }

  });

    



  }


}
