import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Injectable } from '@angular/core';
import { Usuario } from '../_model/usuario';
import { UsuarioDTO } from '../_model/usuatioDTO';
import { Subject } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  usuarioCambio = new Subject<Usuario[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST}/usuarios`;  
  //url: string = `${environment.HOST}/${environment.MICRO_CR}/usuarios`;  
  
  constructor(private http: HttpClient) { }

  obtenerDetalleUsuario(user_name: string) {
    return this.http.get<UsuarioDTO>(`${this.url}?user=${user_name}`);
  }

  registrar(usuario: Usuario) {
    return this.http.post(this.url, usuario);
  }

  obtenerTokenUsuario() {
    let token = sessionStorage.getItem(environment.TOKEN_NAME);
    return token;
  }

  obtenerDetalleUsuarioPorIdCliente(id: number) {
    return this.http.get<UsuarioDTO>(`${this.url}/${id}`);
  }

  listar() {
    return this.http.get<UsuarioDTO[]>(this.url);
  }

}
