import { HttpClient } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Subject } from 'rxjs';
import { Injectable } from '@angular/core';
import { Cliente } from './../_model/cliente';
import { Usuario } from '../_model/usuario';


@Injectable({
  providedIn: 'root'
})
export class ClienteService {
  clienteCambio = new Subject<Cliente[]>();
  mensajeCambio = new Subject<string>();

  url: string = `${environment.HOST}/clientes`;    
  //url: string = `${environment.HOST}/${environment.MICRO_CRUD}/clientes`;    
  
  constructor(private http: HttpClient) { }

  listar() {
      return this.http.get<Cliente[]>(this.url);
  }
  
  obtenerFoto(id: any) { 
    return this.http.get(`${this.url}/obtenerFoto/${id}`, {
      responseType: 'blob'
    });
  }

  registrar(usuario: Usuario, file?: File) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);

    const usuarioBlob = new Blob([JSON.stringify(usuario)], { type: "application/json" });
    formdata.append('usuario', usuarioBlob);

    return this.http.post(`${this.url}`, formdata);
  }

  modificar(usuario: Usuario, file?: File) {
    let formdata: FormData = new FormData();
    formdata.append('file', file);

    const usuarioBlob = new Blob([JSON.stringify(usuario)], { type: "application/json" });
    formdata.append('usuario', usuarioBlob);

    return this.http.put(`${this.url}`, formdata);
  }

  eliminar(id: number) {
    return this.http.delete(`${this.url}/${id}`);
  }

  obtenerUsuarioSistema() {
    return environment.USER_ADMIN_SYSTEM;
  }
}
