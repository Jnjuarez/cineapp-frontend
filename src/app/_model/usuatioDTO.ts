import { Cliente } from './cliente';
export class UsuarioDTO {
    idUsuario: number;
    nombre: string;
    clave: string;
    estado: boolean;
    cliente: Cliente;
}